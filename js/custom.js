// testing url
// var url = 'http://localhost:3000';

// heroku url
var url = 'https://ttt-demo-server.herokuapp.com'


new Vue({
	el: '#app',
	data: {
		name: 'Donovan Griggs',
		gameIsStarted: false,
		board: ["", "", "","", "", "","", "", ""],
		win: false,
		loss: false,
		tie: false
	},
	methods: {
		startGame: function(){
			this.gameIsStarted = true
			this.updateBoard(this.board)
		},
		restartGame: function(){
			
			this.gameIsStarted = true
			this.resetBoard()
			this.win = false,
			this.loss = false,
			this.tie = false
			
		},
		move: function(e){
			if (e.target.innerText === "") {
				var divIndex = e.target.id
				this.board[divIndex] = 'O'
				this.updateBoard(this.board)
				// console.log(e.target)
			}
		},
		updateBoard: function(boardstate){
			axios.post(url,{
				board: boardstate
			}).then((response)  =>  {
				this.board = response.data.board
      })
		},
		resetBoard: function(){
			axios.post(url,{
				board: ["", "", "","", "", "","", "", ""]
			}).then((response)  =>  {
				this.board = response.data.board
      })
		},
	},
	watch: {
		board: function(){
			axios.post(url + '/winner',{
				board: this.board
			}).then((response)  =>  {
				switch(response.data.winner) {
					case "O Wins":
						this.win = true
						break
					case 'Tie':
						this.tie = true
						break
					case "X Wins":
						this.loss = true
						break
				}
      })
		},
	}
})