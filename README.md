# Tic Tack Toe Front End App

## Description

This is a very simple implementation of Vue.js for a tic tac toe game, front end. This application utilizes a tic tac toe API that can be found [here](https://bitbucket.org/donovangriggs/ttt-backend/src/master/). 

**Libraries used**

    Vue.js
    Axios
    Bootstrap

You can find a hosted demo of this application on Heroku, [here](https://ttt-demo-app.herokuapp.com/)

> (Please keep in mind, for demo purposes this is hosted on free tier
> and will sometimes take some time to spin up.)
